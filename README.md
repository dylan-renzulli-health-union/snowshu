# SnowShu
_by Health Union_

![Coverage](https://bitbucket.org/healthunion/snowshu/downloads/coverage.svg)
![Pipeline](https://bitbucket.org/healthunion/snowshu/downloads/pipeline.svg)

SnowShu enables data developers to author transforms and models in a highly performant, fully local environment. It is designed to make true red-green TDD not only possible for data development, but painless and blazingly fast.

![SnowShu Logo](docs/assets/snowshu_logo.png)

*TODO*: need a quick start here! 

Check out the full documentation [here](https://snowshu.readthedocs.org)
